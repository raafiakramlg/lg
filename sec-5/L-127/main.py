#   WE WILL USE TUPLES WITH IN LIST(NESTED TUPLES)


# its a list which contains 5 tuples
albums = [("Welcome to nightmare", "Alice cooper", 1975),
          ("Bad company", "Bad company", 1974),
          ("Nightflight", "Budgie", 1981),
          ("More Mayhem", "Emilda may", 2011),
          ("Ride the lightning", "Metallica", 1984),
          ]

for album in albums:
    name, artist, year = album  # save values from tuple
    print(f"Album : {name} Artist: {artist} Year : {year}")

