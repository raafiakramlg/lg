# WE WILL SEE JOIN METHOD HERE

# a normal list of flower
flowers = [
    "Rose",
    "Tiger lily",
    "Iris",
    "Daffodil"
]

separator = ","
flower_list = separator.join(flowers)
print(flower_list)

print(type(flower_list))
print(id(flowers))
print(id(flower_list))
print(type(flowers))
# separator = "."
# flower_list = separator.join(flower_list)
# print(flower_list)

# list of numbers
# numbers = [1 , 5, 6, 20, 55]  # this will cause an error.because
                                # join only works with string
# print(",".join(numbers))