#   WE WILL DELETE ITEMS THAT ARE HIGHER THAN A PREDETERMINED VALUE FROM A LIST

# create a list of number
numbers = [14, 10, 18, 26, 39, 40, 50, 60, 67, 88, 100, 120, 140, 150, 170, 190]

max_value = 80
position = 0
for index in range(len(numbers) - 1, -1, -1):
    if numbers[index] < max_value:
        position = index + 1
        break

del numbers[position:]
print(numbers)