# DELETE ITEM FROM AN ORDERED LIST

# list of ordered numbers
numbers = [10, 20, 30, 40, 50, 60, 70, 70, 90, 100, 140, 180, 210, 310, 450 ]

min_value = 100

position = 0
for index, value in enumerate(numbers):
    if value > min_value:
        position = index
        break

del numbers[:position]

print(numbers)