# UNPACKING A TUPLE

x, y, z = 5, 9, 100     # right side of equal is tuple
print(type(x))

# unpacking a tuple
data = 9, 6, 123
x, y, z = data
print(x)
print(y)
print(z)