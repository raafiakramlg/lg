# WE WILL SEE HOW MAX, MIN, COUNT, LEN WORKS IN LIST, STRING

#  a list of even integer number
even = [2, 4, 6, 8]

# a list of odd integer number
odd = [1, 3, 5, 7, 9]

# use max and min operation
print("max of even {}".format(max(even)))
print(f"min of even {min(even)}")

print("max of odd {}".format(max(odd)))
print(f"min of odd {min(odd)}")

# use len operation
print(f"length of even {len(even)}")

name = "mississippi"     # declare a string

print(f"len of mississippi {len(name)}")

# use count operation

print(f"count of 's' in 'mississippi' {name.count('s')}")