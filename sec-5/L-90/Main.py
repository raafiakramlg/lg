# WE WANT TO BIND MULTIPLE NAMES TO A SINGLE LIST

# create a simple list
shopping_list = ["milk", "butter", "egg", "meat", "fish"]

a = b = c = d = e = shopping_list   # we can bind multiple variable like this

# append new item in list c
c.append("sugar")

# append new item in list a
a.append("cream")

# we will print all lists here
print(a)
print(b)
print(c)
print(d)
print(e)
print(shopping_list)