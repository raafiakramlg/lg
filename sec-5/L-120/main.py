#   WE WILL USE SPLIT METHOD HERE

pangram = "The quick brown fox, jumps over the lazy fox."    # string variable

print("{}".format(pangram.split()))     # split method. it returns a list

print(f"{pangram.split(',')}")
