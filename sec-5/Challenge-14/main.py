# WE WILL USE NESTED LIST AND TUPLES HERE
# WE WILL MAKE A LIST OF ALBUMS INSIDE THERE WILL BE TUPLE OF SOME ALBUM
# INSIDE THERE WILL BE LIST OF SONGS AND INSIDE WILL BE TUPLE OF SONG DETAILS
# WE HAVE TO USE NESTED INDEXING TO FIND A SPECIFIC SONG HERE


albums = [
    ("Welcome to my Nightmare", "Alice Cooper", 1975,
     [
         (1, "Welcome to my Nightmare"),
         (2, "Devil's Food"),
         (3, "The Black Widow"),
         (4, "Some Folks"),
         (5, "Only Women Bleed"),
     ]
     ),
    ("Bad Company", "Bad Company", 1974,
     [
         (1, "Can't Get Enough"),
         (2, "Rock Steady"),
         (3, "Ready for Love"),
         (4, "Don't Let Me Down"),
         (5, "Bad Company"),
         (6, "The Way I Choose"),
         (7, "Movin' On"),
         (8, "Seagull"),
     ]
     ),
    ("Nightflight", "Budgie", 1981,
     [
         (1, "I Turned to Stone"),
         (2, "Keeping a Rendezvous"),
         (3, "Reaper of the Glory"),
         (4, "She Used Me Up"),
     ]
     ),
    ("More Mayhem", "Imelda May", 2011,
     [
         (1, "Pulling the Rug"),
         (2, "Psycho"),
         (3, "Mayhem"),
         (4, "Kentish Town Waltz"),
     ]
     ),
]

# using nested indexing we are choosing song here
print("print 'The way i choose' from 'Bad company'")
print(albums[1][3][5][1])
print("print the year that the album 'Nightflight was released'")
print(albums[2][2])
print("print the track number of song 'kentish town waltz'from the imedela may album"
      "'More mayhem'")
print(albums[3][3][3][0])
print(albums[2][3][1])