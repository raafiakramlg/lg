# HERE WE WILL DELETE ITEM FROM A LIST FROM BACKWORD

# list of numbers
numbers = [ 5, 8, 19, 34, 97, 45, 76,
            106, 200, 8, 10, 15, 34, 56]

# max and min parameter of numbers we wanna keep in list
min_valid = 50
max_valid = 100

for i in range(len(numbers) - 1, -1, -1):
    # print(i)
    if numbers[i] < min_valid or numbers[i] > max_valid:
        print(i, numbers)
        del numbers[i]

print(numbers)