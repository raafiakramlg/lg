# HERE WE WILL DELETE ITEM FROM A LIST FROM BACKWARD USING REVERSE FUNCTION

# list of numbers
numbers = [5, 8, 19, 34, 97, 45, 76,
            106, 200, 8, 10, 15, 34, 56]

# max and min parameter of numbers we wanna keep in list
min_valid = 50
max_valid = 100
numbers_len = len(numbers) - 1
# We use reversed FUNCTION here
for i, value in enumerate(reversed(numbers)):
    # print(i,value)
    if value < min_valid or value > max_valid:
         print(i, value)
         del numbers[numbers_len - i]

print(numbers)