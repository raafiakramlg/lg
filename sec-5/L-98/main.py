# WE HAVE TO MAKE PREVIOUS LECTURE PROGRAM MORE EFFICIENT. IN CASE YOU FORGOT
# WHAT THE PROBLEM WAS IT'S GIVEN AGAIN BELOW.
# MAKE A LIST OF COMPUTER PARTS LIST. SHOW A LIST OF ITEMS FOR SELLING
# CHOOSE THE ITEMS YOU WANT TO BUY FROM LIST. ADD NEW ITEMS TO YOUR
# LIST. ALSO REMOVE A ITEM IF CUSTOMER WANTS TO REMOVE IT FROM LIST

# create list of computer parts that we can buy here
available_parts = ["computer",
                   "monitor",
                   "keyboard",
                   "mouse",
                   "mouse mat",
                   "hdmi cable",
                   "ssd",
                   "hard disk"
                   ]

computer_parts = []     # empty list where we are gonna save our parts
current_choice = "-"    # String variable for choosing a an option
valid_choices = []      # list for keeping option number

#  set value of valid_choices
for i in range(1, len(available_parts) + 1):
    valid_choices.append(str(i))


while current_choice != "0":
    if current_choice in valid_choices:

        index = int(current_choice) - 1 # find the index of item we want to to add or remove
        part = available_parts[index]   # get the item
        if part in computer_parts:      # if it already exists in list then remove it
            print("Removing choice {}".format(current_choice))
            computer_parts.remove(part)
        else:
            print("Adding choice {}".format(current_choice))
            computer_parts.append(part)     # if its not in list then add it
        print("Your list contains {}".format(computer_parts))   # show all items in list
    else:
        print("Enter option from below : ")
        for number,parts in enumerate(available_parts):
            print("{0}. {1}".format(number + 1, parts))
        print("0. Quit")

    current_choice = input()  # adding a current choice

print(computer_parts)
