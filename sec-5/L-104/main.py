# REPLACING LIST ITEM BY USING SLICE

# take a list of computer parts
computer_parts = ["computer", "keyboard", "mouse", "mouse pad", "sound box"]

computer_parts[3:] = "trackball"
print(computer_parts)

# have to add as a list item to replace it
computer_parts[3:] = ["trackball"]

print(computer_parts)