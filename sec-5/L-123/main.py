# WE WILL SEE TUPLES ARE IMMUTABLE

# create a normal tuple
metallica = ("Ride the lightning", "Metallica", 1984)
print(metallica)
print(metallica[0])     # we can use them same way as list
print(metallica[1])
print(metallica[2])

#metallica[0] = "Master of puppets"      # this line wont work since its immutable

metallica2 = list(metallica)        # put tuple value metallica in a list
print(metallica2)
metallica2[0] = "Master of puppets"
print(metallica2)