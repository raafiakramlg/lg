# we will see mutable object example here


# a simple list
shopping_list = ["milk", "butter", "egg", "meat", "fish"]

another_shopping_list = shopping_list       # save shopping list here

# check their id's
print(shopping_list)
print(id(shopping_list))
print(id(another_shopping_list))
print("-" * 80)

# add a new list in shopping_list
shopping_list += ["cat food"]

print(shopping_list)
# check their id's again after changing shopping list
print(id(shopping_list))
print(id(another_shopping_list))

