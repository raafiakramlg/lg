# ADD 2 LIS TS AND SORT A LIST

# list of even number
even = [2, 4, 6, 8]
# list of odd number
odd = [1, 3, 5, 7, 9]

print(even)
# add 2 lists
even.extend(odd)
# print even after extend
print(even)
# sort even list
even.sort()
print(even)  # print even after sorting

# to sort in reverse order
even.sort(reverse=True)

print(even)     # print after reverse