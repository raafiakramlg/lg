#  HERE WE WILL IMPORT DATA FROM "MAIN"
#  WE WILL MAKE A CODE OF JUKEBOX HERE
#  WE WILL HAVE A LIST OF ALBUMS. WE WILL SELECT A ALBUM AND PLAY
#  A SONG FROM IT
from main import albums

SONG_LIST_INDEX = 3     # constant

while True:
    print("Choose the album you want to play")
    print("Choose 0 to quit")
    for index, (title, artist, year, songs) in enumerate(albums):
        print(f"{index + 1} {title} {artist} {year}")
    choice = int(input())
    if choice == 0 :
        break;
    if 1 <= choice <= len(albums):      # check if choice is within albums limit
        songs_list = albums[choice - 1][SONG_LIST_INDEX]
        for index, (track_number, song) in enumerate(songs_list):
            print(f"{index + 1} {song}")

        print("Enter the song you want to play")
        select_song = int(input())
        if 1 <= select_song <= len(songs_list):
            print("Now playing {}".format(songs_list[select_song - 1][1]))
            print("=" * 50)
        else:
            print("That song does not exists")
    else:
        print("You have chosen an invalid album")