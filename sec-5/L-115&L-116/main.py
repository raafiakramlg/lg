# WE HAVE TO REMOVE "spam" FROM FOLLOWING LIST

# list of food menus
menu = [
    ["egg", "bacon"],
    ["egg", "sausage", "bacon"],
    ["egg", "spam"],
    ["egg", "bacon", "spam"],
    ["egg", "bacon", "sausage", "spam"],
    ["spam", "bacon", "sausage", "spam"],
    ["spam", "sausage", "spam", "bacon", "spam", "tomato", "spam"],
    ["spam", "egg", "spam", "spam", "bacon", "spam"]
]

for foods in menu:
    position = len(foods) - 1       # length of each list in list menu
    for index, item in enumerate(reversed(foods)):  # loop through backward
        if item == "spam":
            #print(foods, position, index)
            del foods[position - index]         # delete item spam from menu



print(menu)