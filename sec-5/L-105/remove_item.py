# REMOVE ITEM FROM A LIST.
# WE WILL SEE UNSAFE WAY HERE

# make a list of numbers

numbers = [5, 30, 130, 500, 234, 121, 777, 553, 987, 34, 50, 120, 77]
print(len(numbers))
# to remove an item from list
del numbers[0:2]
print(numbers)
print(len(numbers))

# But this process is not safe to delete a number

for i, value in enumerate(numbers):
    if value < 100 :
        del numbers[i]
print(numbers)
# here some numbers lesser than 100 are not removed. Its because length of
# numbers are continuously updated. so index of number keeps changing but looping position doesn't change