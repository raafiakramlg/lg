# we will see some immutable built in python

result = True       # set boolean value
another_result = result     # set result in new variable


# print id of result
print(id(result))
print(id(another_result))

result = False
print(id(result))

print("-" * 80)
result = "correct"
another_result = result

print(id(result))
print(id(another_result))

# concat result with new String

result += "ish"

print(id(result))
print(id(another_result))
