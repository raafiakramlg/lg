#   WE USE DEMONSTARTE TUPLE HERE

t = "a", "b", "c"   # we can declare tuple like this
tup = ("a", "b", "c")   # we can also declare like this

print(t, tup)

print(("a", "b", "c"))      # this is also a way to declare tuple
# however to send it like an argument its better to always use '()'
# parenthesis while using tuple