# WE WILL CREATE LISTS EVERY POSSIBLE WAY I KNOW

empty_list = []         # an  empty list
even = [2, 4, 6, 8]     # list of even numbers
odd = [1, 3, 5, 7, 9]   # list of odd numbers

numbers = even + odd    # concat 2 lists in numbers
print(numbers)

# create a new sorted list
sorted_numbers = sorted(numbers)       # sorted is function here

# sorted_numbers and numbers are different list. it's a new list

print("print number list : {}".format(numbers))
print(f"print sorted_number list {sorted_numbers}")

more_numbers = list(numbers)    # another way of creating a new list
more_numbers.sort()

print(f"print more_number list {more_numbers}")
print(f"print number list {numbers}")


# we can use slice to copy list
slice_numbers = numbers[:]      # new copy of list created

# another way is using copy method
copy_list = numbers.copy()

# check between this three list if they are same

print(id(slice_numbers))
print(id(copy_list))
print(id(numbers))
print(slice_numbers == numbers)
print(copy_list == numbers)