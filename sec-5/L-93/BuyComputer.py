# MAKE A LIST OF COMPUTER PARTS LIST. SHOW A LIST OF ITEMS FOR SELLING
# CHOOSE THE ITEMS YOU WANT TO BUY FROM LIST. ADD NEW ITEMS TO YOUR
# LIST


computer_parts= []  # create an empty list o items

current_choice = "-"    # String variable for choosing a an option


while current_choice != "0":
    if current_choice in "123456":
        print("Adding choice {}".format(current_choice))
        if current_choice == "1":
            computer_parts.append("computer")
        elif current_choice == "2":
            computer_parts.append("monitor")
        elif current_choice == "3":
            computer_parts.append("keyboard")
        elif current_choice == "4":
            computer_parts.append("mouse")
        elif current_choice == "5":
            computer_parts.append("mouse mat")
        if current_choice == "6":
            computer_parts.append("hdmi cable")
    else:
        print("Enter option from below : ")
        print("1. Computer")
        print("2. Monitor")
        print("3. KeyBoard")
        print("4. Mouse")
        print("5. Mouse mat")
        print("6. Hdmi cable")
        print("0. Quit")

    current_choice = input()    # adding a current choice

print(computer_parts)