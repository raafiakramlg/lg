# WE HAVE TO MAKE PREVIOUS LECTURE PROGRAM MORE EFFICIENT. IN CASE YOU FORGOT
# WHAT THE PROBLEM WAS IT'S GIVEN AGAIN BELOW.
# MAKE A LIST OF COMPUTER PARTS LIST. SHOW A LIST OF ITEMS FOR SELLING
# CHOOSE THE ITEMS YOU WANT TO BUY FROM LIST. ADD NEW ITEMS TO YOUR
# LIST

# create list of computer parts that we can buy here
available_parts = ["computer",
                   "monitor",
                   "keyboard",
                   "mouse",
                   "mouse mat",
                   "hdmi cable",
                   "ssd",
                   "hard disk"
                   ]

computer_parts = []     # empty list where we are gonna save our parts
current_choice = "-"    # String variable for choosing a an option
valid_choices = []      # list for keeping option number

#  set value of valid_choices
for i in range(1, len(available_parts) + 1):
    valid_choices.append(str(i))
print(valid_choices)
while current_choice != "0":
    if current_choice in valid_choices:
        print("Adding choice {}".format(current_choice))
        index = int(current_choice) - 1
        computer_parts.append(available_parts[index])
    else:
        print("Enter option from below : ")
        for parts in available_parts:
            print("{0}. {1}".format(available_parts.index(parts) + 1, parts))
        print("0. Quit")

    current_choice = input()  # adding a current choice

print(computer_parts)
