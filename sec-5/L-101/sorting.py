#  WE WILL USE SORT FUNTION HERE

# String variable
pangram = "The quick brown fox jumps over the lazy dog."

letters = sorted(pangram)

print(letters)
# a list of float numbers
numbers = [ 4.7, 1.6, 8.8, 3.9, 7.9, 1.0, 6.7, 0.8]
sorted_numbers = sorted(numbers)    # sorted_numbers is a new list using sorted function

# print both of the lists
print(sorted_numbers)
print(numbers)

# use sort method here
numbers.sort()
print(numbers)

missing_letters = sorted("The quick brown fox jumps over the lazy dog.")
print(missing_letters)

sorted_letter = sorted(pangram)
print(sorted_letter)