# implementation of simple list


# list on computer parts
computer_parts = ["computer", "monitor", "mouse", "keyboard", "mouse pad"]
# forSlicingPosition= -5          -4        -3         -2         -1   (Negative indexing)


# loop through computer_parts
for part in computer_parts:
    print(part)
print("-" * 80)

# print the part of computer of a specific index
print(computer_parts[3])
print("-" * 80)

# print index of a part
print(computer_parts.index("mouse"))
print("*" * 80)

# use slice on computer parts
print(computer_parts[:3])
print(computer_parts[-3])