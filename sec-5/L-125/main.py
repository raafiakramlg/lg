# WE WILL SEE PRATICAL EXAMPLE OF TUPLE
# USING ENUMERATE FROM LOOP

for index, character in enumerate("abcdefgh"):
    print(index, character)

# we will use tuple for same for loop
for t in enumerate("abcdeghh"):
    print(t)

t = 0, 'a'
index, character = t    # same thing as loop
print(index, character)
