#   WE WILL SEE CASE INSENSITIVE SORTING

# String variable
line = "Hello, I'm gonna sort it including upper and lower case"

sorted_line = sorted(line, key=str.casefold)
print(sorted_line)

# create a list of names
names = ["tom",
         "Harry",
         "Don",
         "gem",
         "eric",
         "atom"
         ]

# using sort method ignoring case sensitivity
names.sort(key=str.casefold)
print(names)
