# CHALLENGE.
# WE GET A LIST OF NUMBERS AND THEIR TYPE IS STRING. WE HAVE TO CONVERT THEM
# INTO INTEGER

value_list = ["9", " "
              "2", "2", "3", " ",
              "3", "7", "2", " ",
              "0", "3", "6", " ",
              "8", "5", "4", " ",
              "7", "7", "5", " ",
              "8", "0", "7",
              ]
values = "".join(value_list)    # first make a string
number_list = values.split(" ") # make a list again
print(number_list)

answer = []
for value in range(len(number_list)):
    answer.append(int(number_list[value]))
print(answer)