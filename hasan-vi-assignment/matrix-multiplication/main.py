# MATRIX MULTIPLICATION

##########################################
##########################################
# matrix input
# take input of matrix A

matrix_a = []
matrix_b = []

# take row and column for matrix_a
row = int(input("take row for matrix_a : "))
column = int(input("take column for matrix_a : "))

for i in range(row):
    a = []     # empty list
    for j in range(column):
        val = input()
        a.append(val)
    matrix_a.append(a)

# take row and column for matrix_a
row = int(input("take row for matrix_b : "))
column = int(input("take column for matrix_b : "))

for i in range(row):
    a = []     # empty list
    for j in range(column):
        val = input()
        a.append(val)
    matrix_b.append(a)


###########################################
###########################################
# matrix multiplication
answer = []
# print("len a{}".format(len(matrix_a)))
# print("len b{}".format(len(matrix_b)))
if len(matrix_a[0]) == len(matrix_b):
    for i in range(0, len(matrix_a)):
        temp = []
        for j in range(0, len(matrix_b[0])):
            sum = 0
            for k in range(0, len(matrix_b)):
                val1 = int(matrix_a[i][k])
                val2 = int(matrix_b[k][j])
                sum += val1 * val2
            temp.append(sum)
        answer.append(temp)
        j = 0
print(answer)


