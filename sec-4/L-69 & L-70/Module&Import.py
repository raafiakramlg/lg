# we will generate a random variable using random module and import it
# we will guess the random variable in this problem


import random

highest = 10  # a variable set for ranging
answer = random.randint(1, highest)  # randomly select a value

# print(answer)        TODO : comment the line after testing
guess = None  # a input variable to guess the answer
print("Guess a number in a range of 1 - {} ".format(highest))

# use loop to compare between answer and guess
while guess != answer:
    guess = int(input())
    if guess > answer:
        print("You have guessed higher")
    elif guess < answer:
        print("You have guessed it lower")
print("Congratulation you have guessed it correctly")
