# cont. of guessing game
# we will take a value from input and compare it with a predetermined number


answer = 5  # its an integer value that ww will compare with a value taken from input

guess = int(input("Take a guess from 1 - 10 : "))       # its the value we will compare with answer

# compare 2 values here
if answer > guess :
    print("your guess is a bit lower.")
    guess = int(input("Take a guess from 1 - 10 : "))
    if answer == guess:
        print("Congratulations!! you have guessed it correctly this time..")
    else:
        print("Sorry, you have not guessed it correctly..")
elif answer < guess :
    print("your guess is a bit higher.")
    guess = int(input("Take a guess from 1 - 10 : "))
    if answer == guess:
        print("Congratulations!! you have guessed it correctly this time..")
    else:
        print("Sorry, you have not guessed it correctly..")
else:
    print("Congratulations!! you have guessed it correctly in your first try..")