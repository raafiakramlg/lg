# USING AUGMENTATION MULTIPLY THE NUMBER AND MULTIPLIER

number = 5      # given integer value
multiplier = 8  # integer value
answer = 0      # result

# loop through multiplier
for i in range(multiplier):
    answer += number

print(answer)
