# Ranges cont.
# will use range in different situation

# normal range
for i in range(1, 5):
    print(i)
print("-" * 80)

# range without stating starting point it starts from 0
for i in range(10):
    print(i)
print("-" * 80)

# range using step
for i in range(0, 10, 2):
    print(i)
print("-" * 80)

# negative range
for i in range(10, 0, -2):
    print(i)
print("-" * 80)

# range in condition
# BUT WE SHOULD NOT USE IT
age = 10
if age in range(5,11):
    print("you are in")