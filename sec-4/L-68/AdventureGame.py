# previous lecture problem.
# we need to make a list of exit path from a maze. until we find exit we keep searching in maze
# we need to give a quit option for players here.

# exit paths list from maze
available_exits = ["east", "west", "north", "south"]

current_location = ""   # variable for taking string input

print("Type quit to stop the game")
# search path in while loop

while current_location not in available_exits:
    current_location = input("Please choose an exit path ")
    if current_location.casefold() == "quit":
        print("Game over")
        break

else:
    print("Aren't you glad getting out of there alive!!")