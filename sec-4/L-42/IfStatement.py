# implementation of condition
# program for checking a person old enough to vote

name = input("Please enter your name ")  # name is a String variable. Taken from terminal
age = int(input (f"Please enter your age,{name} "))   # age is an integer variable. Taken from terminal

# Check if the person old enough

if age >= 18 :
    print("You are old enough to vote")

else :
    print(f"You can vote in next {18-age} year")    # Used f-String format here
#    print("You can vote in next {0} year".format(18 - age))    # Used normal format here.


