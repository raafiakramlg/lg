# a program on guessing a number
# further demonstrate of conditions

answer = 5      # an integer variable. its the hidden number we want to guess

guessingNumber = int(input("Enter a number between 1 - 10 : "))        # an int variable taken from terminal to compare between answer

# compare between guessingNumber and answer

if guessingNumber > answer :
    print("You have guessed a bit higher")
elif guessingNumber < answer :
    print("You have guessed a bit lower")
else :
    print("Congratulation!! you have guessed it correctly..")