# implementation of continue here
# Code finds continue next codes in THAT block skips

# make a simple list
shop_list = ["butter", "milk", "meat", "spam", "bread", "egg"]

for item in shop_list:
    if item == "spam":  # here spam item skipped. continue starts it from the start of loop
        continue
    print("Buy " + item)

count = 0
while count < 10:
    if count == 5:
        break
    elif count < 4:
        print("4")
    else:
        print("else")
    print(count)
    count += 1

else:
    print("hi")


