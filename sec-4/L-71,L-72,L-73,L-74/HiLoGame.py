# we will use binary search here
# computer will guess a random number and we will find it

import random

answer = random.randint(1, 1000) # random result we want to find

hi = 1000       # highest range is 1000
lo = 1          # lowest range is 1
guesses = 0     # counting guesses
mid = 0         # mid value

while hi > lo :
    mid = ((hi + lo) // 2)

    if mid > answer :
        hi = mid
    elif mid < answer :
        lo = mid
    else:
        print("you found the number.")
        break

    guesses += 1

print(f"You took {guesses} guesses to find answer {answer}")
