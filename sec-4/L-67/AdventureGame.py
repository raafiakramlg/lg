# another demonstrate of while loop
# we need to make a list of exit path from a maze. until we find exit we keep searching in maze

# exit paths list from maze
available_exits = ["east", "west", "north", "south"]

current_location = ""   # variable for taking string input

# search path in while loop

while current_location not in available_exits:
    current_location = input("Please choose an exit path ")

print("Aren't you glad getting out of there alive!!")