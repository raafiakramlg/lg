# WE WILL SEE ELSE USE IN FOR LOOP
# WE WILL TAKE A LIST OF INTEGERS CHECK SOME CONDITIONS IF IT DOES
# NOT BREAK IT WILL HIT ELSE CONDITION


# list of numbers
numbers = [5, 19, 33, 60, 90]

for i in numbers :
    #print(i)
    if i % 8 == 0:
        print("Number {} is divided by 8".format(i))
        break
else:
    print("8 can not divide them")

