# implementation of boolean expression
# we are gonna take some boolean variable and test conditions with them


day = "Saturday"    # String variable
temperature = 30    # integer variable
raining = False      # boolean variable

#   Check conditions if all the variables are true
if day == "Saturday" and temperature > 25 and raining:
    print("Go to swimming")
else:
    print("Learn python")


#   check with 'or' operation
day = "Friday"
temperature = 20
raining = False
if (day == "Saturday" and temperature > 25) or not raining:
    print("Go to swimming")
else:
    print("Learn python")