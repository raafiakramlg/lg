# We are gonna take a letter as input and check if it exists in a given String
# we will use 'in' operation here


parrot = "Norwegian Blue"       # a String variable. we will take a letter and comapre with this string
letter = input("Enter a character ")     # we will see if it exits in parrot

# use condition here to check if the letter exist in parrot
if letter in parrot:
    print("{0} is in {1}".format(letter,parrot))
else:
    print("We don't need that letter")


# We will demonstrate not in operation here

activity = input("Where do you want to go? ")    # String variable

if "cinema" not in activity:
    print("But i want to go to cinema")

# here cinema is case sensitive. if we want to make activity
# in lower character then use 'casefold' operation

# casefold will convert all the letter in activity to lower case to check wit "cinema"
if "cinema" not in activity.casefold():
    print("But i want to go to cinema")
else:
    print("Yay..")