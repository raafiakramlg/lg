# will update previous problem here
# previous problem was we will take a list of shop item and search through them for a item

# make a normal list
shop_list = ["egg", "meat", "fish", "spam", "vegetables", "butter"]

found_at = None     # here None means null

# for i in range(len(shop_list)):
#     if shop_list[i] == "spam":
#         found_at = i
#         break   # when item found no need to continue searching. So we break here.

# we will change this part of code

if "spam" in shop_list:
    found_at = shop_list.index("spam")

print("Item {0} found at {1} index".format("spam", found_at + 1))