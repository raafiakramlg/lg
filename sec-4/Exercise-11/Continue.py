# WRITE A PROGRAM THAT PRINTS ALL BETWEEN 0 TO 20 THAT ARE NOT DIVISIBLE BY 3
# OR 5. 0 SHOULD NOT BE INCLUDED AS SOLUTION

for i in range (1,20):
    if (i % 3 is 0) or (i % 5 is 0):
        continue
    print(i)