# we are gonna improve previous code
# just to remember gonna write the problem again
# we need to take a value from input and compare it with predetermined value


answer = 5  # integer variable which will be compared with input value
guess = int(input("Guess a value between 1 - 10 : "))   # this is the value we will compare with answer

# compare between guess and answer
if answer != guess:
    if answer < guess:
        print("You have guessed more than answer")
    else:
        print("You have guessed less than answer")
    guess = int(input())
    if answer == guess:
        print("Congratz!! You have guessed it correctly this time")
    else:
        print("You didn't guess it correct")
else:
    print("Congratz!! You have guessed it correctly in first time")