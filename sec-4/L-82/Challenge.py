# MAKE A PROGRAM WHERE USERS CAN CHOOSE A OPTION AND IF THE OPTION IS
# 0 THEN PROGRAM SHOULD BREAK. PRINT THE OPTION HE HAS CHOSEN. IF AN
# INVALID OPTION IS CHOSEN OPTION MENU PRINTED AGAIN. THERE SHOULD BE
# NO DUPLICATED CODE

option = None   # option that will be taken as a input

while option != 0:
    if option is None:
        print("Please choose option from list below")
        print("1. Learn python")
        print("2. Learn JAVA")
        print("3. Time to eat")
        print("4. Time to workout")
        print("5. Go to sleep")
        print("0. quit")
    option = int(input())
    print("You have chosen option {}".format(option))   # for valid input show option number
    if 0 > option or 5 < option:        # for invalid input
        print("Invalid option")
        option = None

