# Its a challenge on conditions
# take a input of name and age
# if their age is over 18 and under 31 then they can join in holiday event


name = input("Enter your name: ")       # String variable that bounds a name
age = int(input("How old are you? "))   # integer variable that bounds a age

# check condition if its within age restrictions
if 18 <= age < 31:
    print("Welcome to 18-30 holiday, {0}".format(name))
else:
    print("Sorry. its only for 18 - 30")