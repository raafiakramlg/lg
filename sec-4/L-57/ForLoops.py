# use for loop in this code

parrot = "Norwegian Blue"   # String variable

# we will use a for loop to print through parrot
for character in parrot:
    print(character)
