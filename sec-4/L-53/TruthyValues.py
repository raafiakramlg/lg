# we well test whether integer '0' and an empty string is true or false

if 0:
    print("True")
else:
    print("False")

name = input("Please enter a name")     # take a String input here
# compare whether the name is empty string or not
if name:
    print("hello, {0}".format(name))
else:
    print("Are you nameless?")