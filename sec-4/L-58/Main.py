# we will loop through a String and save the non-numeric chars in another String

number = "9,892;574'859,534;857{348}984 853.444/453"    # a string variable in which we set a number
seperator = ""      # a variable where we will save non digit chars from number

# now loop through the number and find non-digits
for char in number:
    if not char.isnumeric():
        seperator = seperator + char

print(seperator)