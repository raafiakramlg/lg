# we will see some augmented assignment here

x = 10  # initialize a integer variable

x += 1
print(x)        # 11

x -= 1
print(x)        # 10

x *= 2
print(x)        # 20

x /= 2
print(x)        # 10.0
print

x %= 2          # 0.0
print(x)

x = 13
x //= 2         # 6
print(x)

