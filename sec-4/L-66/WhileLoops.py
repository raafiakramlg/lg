# implement while loop here
# we will first make a for loop and then make a while loop with exact functionality


# a for loop to print through 1 - 10

for i in range (10):
    print("i is now {0} in for loop".format(i))

print("-" * 50)

# a while loop with exact output of previous for loop

i = 0   # initialize a starting value in integer i variable

while i < 10:       # condition given that i will be smaller than 10
    print("i is now {0} in while loop".format(i))
    i += 1
