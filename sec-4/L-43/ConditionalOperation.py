# we will make a code to check a person is old enough to vote
# we will implement elif here

name = input("Please enter your name : ")   # "name" is a string variable here
age = int(input(f"Please enter your age, {name} "))  # "age" is a integer variable

# condition to check age
if age < 18 :               # if age is smaller than 18
    print(f"You are not old enough, please come back within {18-age} years")       # used f-String format here
elif age >= 150 :           # if age is greater than 149 then it should be invalid input
    print("Please reenter your age again.")
else :                      # age is greater than 17 but smaller than 150
    print("Yes you are eligible to vote")