# We will try to use simplify condition in this code


age = int(input("How old are you? "))       # integer variable which is bound by input from terminal

if 16 <= age <= 65:
    print("Have a good day at work")
else:
    print("Enjoy your free time")

# we will use or condition to generate same output

if age < 16 or age > 65:
    print("Enjoy your free time")
else:
    print("Have a good day at work")