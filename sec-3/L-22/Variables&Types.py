# See variables and its types

age = 24            # variable
print(age)
print(type(age))    # variable type integer

age = "2 years"     # String type variable
print(age)
print(type(age))
