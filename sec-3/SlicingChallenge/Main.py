# Challenge
# make a string of letters from 'a' to 'z'
# create a slice that produces character qpo
# Slice the string to create 'edcba'
# Slice the String to produce last 8 characters in reverse order


letters = "abcdefghijklmnopqrstuvwxyz"      # a String that contains 'a' to 'z'
print(letters[16 : 13 : -1])
print(letters[4 : : -1])
print(letters[25 : 17 : -1])
