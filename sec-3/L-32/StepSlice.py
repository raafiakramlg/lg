# Slice string using step

parrot = "Norwegian Blue"       # String type variable
print(parrot[0: 6: 2])  # (Nre) starts with N and takes 2 steps right
print(parrot[0: 6: 3])  # (Nw) takes 3 steps right
print(parrot[:-1])