# String test

print("Today is a good day to learn python")
print("Python is fun")
print("Python's string is easy to use")
print('We can even include "quotes" here')

# String concat
print("Hello " + "world")

greeting = "Hello"
# To take input from keyboard
name = input("Please enter your name ")
print(greeting + " " + name)



