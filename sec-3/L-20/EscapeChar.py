# Implement escape character


# new line test
splitString = "This string\nhas been\nsplit in several\nlines"
print(splitString)

# tab test
tabbedString = "1\t2\t3\t4\t5"
print(tabbedString)

print('The pet shop owner said,"No, he\'s sleeping".')
print("""The pet shop owner said,"No, hes's sleeping".""")