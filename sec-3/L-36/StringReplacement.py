# gonna implement String operation (format) here

age = 24        # variable

# convert Integer to String and concat with string
print("Hello my age is " + str(age) + " years.")

# use format here
print("Hello my age is {0} years.".format(age))

print("The months that has {0} days in every years are - {1} {2} {3} {4} {5} {6} {7}"
      .format(age,"jan","march","may","july","august","oct","dec"))

print("The days are in months jan {2}, feb {0}, mar {2}, apr {1}, may {2}, jun {1}, july {2}, aug {2}"
      "sep {1}, oct {2} nov {1} dec {2}".format(28,30,31))