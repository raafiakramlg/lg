# we are gonna loop through a range of number and square and cube them all

for i in range(1 , 13):
    print("No. {0:2}'s square is {1:3} , cube is {2:4}".format(i, i ** 2, i **3))

print()

# to start numbers from left to right
for i in range(1 , 13):
    print("No. {0:2}'s square is {1:<3} , cube is {2:<4}".format(i, i ** 2, i **3))

print("Pi is approximately {0:12}".format(22 / 7))
print("Pi is approximately {0:50.50f}".format(22 / 7))
print("Pi is approximately {0:12.50f}".format(22 / 7))
print("Pi is approximately {0:72.50f}".format(22 / 7))