# Slicing test for string

parrot = "Norwegian Blue"       # String type variable
print(parrot[2:6])
print(parrot[10:14])

# Negative index slicing

print(parrot[-4 : -1])

