# String implementation
# below will be string index
#                   1
#         01234567890123
parrot = "Norwegian Blue"      # variable String type

# Challenge
# print "we win" using characters of parrot
# each chars should take a single line

print(parrot[3])
print(parrot[4])
print()
print(parrot[3])
print(parrot[6])
print(parrot[8])