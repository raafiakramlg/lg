# String manipulation

string1 = "He's "
string2 = "probably "
string3 = "pining "
string4 = "for the "
string5 = "fjords"

# String concat
print(string1 + string2 + string3 + string4 + string5)

#multiply String
print("Hello " * 5)

# check 'in' operation
today = "Friday"        # String type variable
print("day" in today)
print("Fri" in today)
print("thur" in today)
print("Hello" in "Fjord")