# testing print method

print(4 + 7)       # 11
print(5 * 8)       # 40

# KEEP IN MIND..
# here only arguments are sent. its not a String concat.
# take a look at output. By default space is given after end of every arguments
print("This is end.", "Or is it?", "Python", 3, "has just began")