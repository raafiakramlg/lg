# Expressions

a = 12      # here 12 is expression. a is a variable which is bound by expression

for i in range (1,3):
    print(i)

# here 1 and 2 is expression in loop
