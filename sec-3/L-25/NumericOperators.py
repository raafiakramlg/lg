# we will try most of the numeric operators

a = 12          # 12 bound to variable a
b = 3           # variable

print(a + b)    # 15
print(a - b)    # 9
print(a * b)    # 36
print(a / b)    # 4.0
print(a // b)   # 4(rounded to lower integer)
print(1 // 2)   # 0
print(3 // 2)   # 1
print(a % b)    # 0