# Operator precedence

a = 12      # variable
b = 3       # variable
print(a + b / 3 - 4 * 12)   # -35

# to get 12
print((((a + b) / 3) - 4) * 12)     # 12

