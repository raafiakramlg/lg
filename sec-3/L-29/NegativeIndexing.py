# Challenge
# print "we win" using characters of parrot
# each chars should take a single line
# in negative indexing -1 starts from e
#             1
#         43210987654321
parrot = "Norwegian Blue"       # String type variable
print(parrot[-11])
print(parrot[-1])
print()
print(parrot[-11])
print(parrot[-8])
print(parrot[-6])