#   WRITE A FUNCTION FOR FACTORIAL 0 TO 35


def factorial(num: int) -> int:
    """
    A function for printing factorial numbers
    :param num: numbers given as parameter
    :return: `int` type
    """
    if 0 <= num <= 1:
        return 1
    return factorial(num - 1) * num


for i in range (0, 36):
    print(f"{i} {factorial(i)}")