# WE WILL MAKE A FUNCTION TO CHECK IF A STRING IS PALINDROME


def is_palindrome(s):
    length = len(s)
    for i in range(int(length / 2)):
        if s[i].casefold() != s[length - 1 - i].casefold():
            return False
    return True


print(is_palindrome("jiii"))


def another_is_palindrome(s):
    # print(s[::-1])
    return s[::-1].casefold() == s.casefold()


print(another_is_palindrome("Pop"))
