# WE WILL CREATE A FUNCTION TO CALCULATE FIBONACCI NUMBER


def func_fibonacci(number):
    """
    It is a function that calculates fibonacci
    :param number: Takes `int` as input. It shows which fibonacci we want to print
    :return: Returns "number"th `int` fibonacci value
    """
    if 0 <= number <= 1:
        return number
    return func_fibonacci(number - 1) + func_fibonacci(number - 2)


print(func_fibonacci(4))