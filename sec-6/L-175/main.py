# ADDITION TO PREVIOUS CHALLENGE
# MAKE A FUNCTION FOR FIZZ- BUZZ GAME. GAME GOES LIKE THIS-
# IF A NUMBER IS DIVISIBLE BY 3 THEN PRINT 'FIZZ', IF IT'S DIVISIBLE BY 5
# PRINT 'BUZZ', IF DIVIDED BY BOTH PRINT 'FIZZ-BUZZ'. OTHERWISE PRINT THE NUMBER
# A PLAYER WILL GUESS THE NUMBER FROM INPUT. IF IT GOES WRONG PROGRAM ENDS


def fizz_buzz(num: int) -> str:
    """
    Function for checking fizz buzz number
    :param num: takes integer value as input
    :return: returns string
    """
    if num % 15 == 0:
        return "fizz buzz"
    if num % 5 == 0:
        return "buzz"
    if num % 3 == 0:
        return "fizz"
    else:
        return str(num)


i = 0
while i < 100:
    i += 1
    print(i)
    players_guess = input("You go: ") # player guesses if its fizz buzz
    if fizz_buzz(i) != players_guess:
        print("You have answered wrong")
        break
else:
    print("Well done, you won the game")