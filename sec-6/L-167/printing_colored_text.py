#   WE WILL PRINT CODE WITH COLOR

print('\u001b[31m', "Hello, i'm rad")
print('\u001b[32m', "Hello, i'm green")
print('\u001b[33m', "Hello, i'm yellow")


def color_func(text: str, effect: str) -> None:
    """
    Function for printing text with color
    :param text: text given for printing
    :param effect: color effect
    """
    print(f" {effect}{text}")


color_func("Hello, from color_func as blue", "\u001b[34m")