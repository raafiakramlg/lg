# WE WILL SEE HOW "args" WORKS HERE

numbers = (1, 2, 3, 4, 5)

print(*numbers)  # here * is doing unpacking for us
print(numbers)


def func(*args):    # here its packing values for us
    print(args)
    print(*args)


func(1, 2, 3, 4, 5)
