#   WE WILL SEE FUNCTION TYPE ANNOTATION HERE


def palindrome(word : str) -> bool: # this is function annotation
    """
    this function will check if a word is palindrome or not
    :param word: `string` type
    :return: `boolean` type
    """
    return word[::-1].casefold() == word.casefold()


print(palindrome("hih"))