# USE PALINDROME CHECK ON A SENTENCE

def is_palindrome(s):
    s = str.lower(s)    # lower all cases in string
    string = ""
    for i in range(len(s)):
        if 'a' <= s[i] <= 'z':
            string = s[i]
    return string[::-1] == string


# this is udemy solution
def is_sentence_palindrome(s):
    sentence = ""
    for char in s:
        if char.isalnum():  # isalnum() checks if its an alphanumeric number
            sentence += char
    return is_palindrome(sentence)


print(is_palindrome("Do geese see god?"))
