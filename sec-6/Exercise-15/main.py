# CHALLENGE
# MAKE A FUNCTION FOR FIZZ- BUZZ GAME. GAME GOES LIKE THIS-
# IF A NUMBER IS DIVISIBLE BY 3 THEN PRINT 'FIZZ', IF IT'S DIVISIBLE BY 5
# PRINT 'BUZZ', IF DIVIDED BY BOTH PRINT 'FIZZ-BUZZ'. OTHERWISE PRINT THE NUMBER


def fizz_buzz(num: int) -> str:
    """
    Function for checking fizz buzz number
    :param num: takes integer value as input
    :return: returns string
    """
    if num % 15 == 0:
        return "fizz buzz"
    if num % 5 == 0:
        return "buzz"
    if num % 3 == 0:
        return "fizz"
    else:
        return str(num)


for i in range(1, 101):
    print(fizz_buzz(i))