# WRITE A FUNCTION TO CALCULATE THE SUM OF ALL NUMBERS THAT ARE PASSED AS ARGUMENTS


def sum_numbers(*args: float) -> float:
    """
    function that returns sum of all arguments that are passed
    :param args: `float`
    :return: `float`
    """
    return sum(args)


print(sum_numbers(1, 2, 3))
print(sum_numbers(8, 20, 2))
print(sum_numbers(12.5, 3.147, 98.1))
print(sum_numbers(1.1, 2.2, 5.5))
