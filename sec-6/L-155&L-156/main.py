# WE WILL SEE DEFAULT PARAMETER AND KEYWORD ARGUMENT HERE


def func_multiply(val=4):  # default parameter. val with a default value 4
    """
    This is a function which has default `int` parameter
    :param val: 4
    :return: an integer multiplied val and 1
    """
    return val * 1


help(func_multiply())


def func(child1, child2, child3):
    print(child1, child2, child3)


result = func_multiply()
func(child3="tom", child1="rom", child2="tim")  # watch carefully
# here keyword argument is used. This way the order of the arguments does
# not matter.
