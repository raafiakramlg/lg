# WE WILL USE FUNCTION CALL, ARGUMENT AND PARAMETER


# a function that takes 2 parameters
# and return multiplied value
def multiply(x, y):
    return x * y


print(multiply(10.4, 5))  # send a float and a integer as arguments

print(multiply(4, 8))  # send 2 integers as arguments
