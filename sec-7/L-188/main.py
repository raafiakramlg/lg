# WE WILL USE UPDATE AND COPY METHOD OF DICTIONARY HERE

# a dictionary containing different fruits
fruit = {"apple": "A red and round fruit",
         "orange": "A juicy, tasty and citrus fruit",
         "grape": "A small and sweet fruit",
         "lemon": "A yellow and citrus fruit",
         "lime": "A green, citrus fruit",
         }

# a dictionary containing different veggies
veg = {"cabbage": "good for eye",
       "spinach": "it's not vegetable",
       "radish": "white vegetable",
       }

#veg.update(fruit)

#print(veg)
#print(fruit)

new_fruit = fruit.copy()
new_fruit.update(veg)
print(new_fruit)
print(fruit)
print(veg)