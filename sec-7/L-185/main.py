# WE WILL USE DICTIONARY HERE
# WE WILL CREATE A DICTIONARY OF FRUIT. WE WILL ADD AND REMOVE FRUIT FROM HERE


# create dictionary fruit
fruit = {"apple": "A red and round fruit",
         "orange": "A juicy, tasty and citrus fruit",
         "grape": "A small and sweet fruit",
         "lemon": "A yellow and citrus fruit",
         "lime": "A green, citrus fruit",
         }

print(fruit)
fruit["banana"] = "A yellow, sweet fruit"  # add new fruit in dict.
print(fruit)

# if we add "apple" again previous entry will be overridden
fruit["apple"] = "A sweet and crunchy fruit"
print(fruit)

# delete an entry
del fruit["lime"]
print(fruit)

while True:
    new_entry = input("Enter fruit name : ")
    if new_entry == "quit":
        break
    print(fruit.get(new_entry, "This fruit is not in dictionary"))

# to sort key values of dictionary
ordered_key = list(fruit)
ordered_key.sort()
print(ordered_key)

# or

ordered_key = sorted(list(fruit))
print(ordered_key)

# use built in "keys" and "values" module
print(fruit.keys())
print(fruit.values())
