# WE WILL SEE SET HERE

pet = {"dog", "cat"}
wild_animal = set({"tiger", "lion"})

print(pet)
print(type(wild_animal))
print(wild_animal)

# to add a new animal in pet
pet.add("parrot")
print(pet)

# if we want to create a new set we cant use "{}" cause it will create dictionary
create_set1 = {}
create_set2 = set()
print(type(create_set1))
print(type(create_set2))

# set of even numbers
even = set(range(0, 40, 2))
print(len(even))
print(even)

# set of square numbers
t = (4, 9, 16, 25, 36)
square = set(t)  # convert from tuple to set
print(len(square))
print(square)

# use union method
print(even.union(square))
print(len(even.union(square)))
print(square.union(even))
print(even)

# intersection
print(even & square)
print(even.intersection(square))
print(square.intersection(even))

# subtraction
print(sorted(even.difference(square)))
print(sorted(square.difference(even)))
print(even - square)
print(square.difference(even))

# subtract and update at the same time
print("using difference_update")
print(sorted(even))
print(len(even))
even.difference_update(square)
print(sorted(even))
print(len(even))

# using symmetric_difference. it's opposite of intersection
even = set(range(0, 40, 2))
print("using symmetric difference we use even - square")
print(sorted(even.symmetric_difference(square)))

# delete item from set
square.remove(4)
square.discard(4)
print(square)
# we can't remove a item that does not exist with 'remove' operation
# we use try except in that case
try:
    square.remove(4)
except KeyError:
    print("Item is not in the set")


even = set(range(0, 40, 2))
t = (4, 16, 25)
square = set(t)
print(even.issuperset(square))
square = {4, 16}
print(even.issuperset(square))
print(square.issubset(even))

# frozen set
even = frozenset(range(0, 40, 2))
# even.and(40) it will cause error