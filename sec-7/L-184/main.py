# WE WILL USE DICTIONARY HERE
# WE WILL CREATE A DICTIONARY OF FRUIT. WE WILL ADD AND REMOVE FRUIT FROM HERE


# create dictionary fruit
fruit = {"apple": "A red and round fruit",
         "orange": "A juicy, tasty and citrus fruit",
         "grape": "A small and sweet fruit",
         "lemon": "A yellow and citrus fruit",
         "lime": "A green, citrus fruit",
         }

print(fruit)
fruit["banana"] = "A yellow, sweet fruit"   # add new fruit in dict.
print(fruit)

# if we add "apple" again previous entry will be overridden
fruit["apple"] = "A sweet and crunchy fruit"
print(fruit)

# delete an entry
del fruit["lime"]
print(fruit)

# delete all entry of fruit
# fruit.clear()
# print(fruit)
# print(fruit["tomato"])    # it will cause error. because tomato does not exist.
print(fruit.get("tomato"))  # using "get" will not cause error even if item doesn't exist in dict.
while True:
    new_entry = input("Enter fruit name : ")
    if new_entry == "quit":
        break
    if new_entry in fruit:
        print(fruit.get(new_entry))
    else:
        print("This fruit is not in dictionary.")