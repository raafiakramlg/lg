# Create a program that takes some text and returns a list of
# all the characters in the text that are not vowels, sorted in
# alphabetical order.
#
# You can either enter the text from the keyboard or
# initialise a string variable with the string.


def solve(text: str) -> str:
    result = set()
    # set of vowels
    vowel_set = {'a', 'e', 'i', 'o', 'u'}
    text = text.lower()
    for char in text:
        if char not in vowel_set: # search if char is not a vowel
            result.add(char)
    print(result)
    return sorted(result)


# take a text from input
sentence = input("Enter the text here : ")
print(solve(sentence))

#############################################################
#############################################################
# solved in video. my solution is a dumb one :`(

vowels = frozenset("aeiou")
sample_text = "i am learning python"
final_set = set(sample_text).difference(vowels)
print(sorted(final_set))