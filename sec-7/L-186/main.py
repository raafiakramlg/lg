# WE WILL USE DICTIONARY HERE
# WE WILL CREATE A DICTIONARY OF FRUIT. WE WILL ADD AND REMOVE FRUIT FROM HERE
# WILL SEE FURTHER USE OF DICTIONARY

# create dictionary fruit
fruit = {"apple": "A red and round fruit",
         "orange": "A juicy, tasty and citrus fruit",
         "grape": "A small and sweet fruit",
         "lemon": "A yellow and citrus fruit",
         "lime": "A green, citrus fruit",
         }

print(fruit.items())
# make tuple of fruit dictionary
f_tuple = tuple(fruit.items())
print(f_tuple)

for key, description in f_tuple:
    print(key, description)

# convert it to dictionary
print(dict(f_tuple))