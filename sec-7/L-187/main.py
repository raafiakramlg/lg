# WE WILL MAKE A ROAD MAP NAME "locations". AND WILL MOVE THROUGH MAP
# IT IS FROM PREVIOUS LECTURE. WE HAVE TO REPLACE EXITS THAT WERE WRITTEN WITH LIST
# WITH DICTIONARY

# dictionary named locations
locations = {0: "You are sitting in front of a computer learning Python",
             1: "You are standing at the end of a road before a small brick building",
             2: "You are at the top of a hill",
             3: "You are inside a building, a well house for a small stream",
             4: "You are in a valley beside a stream",
             5: "You are in the forest"}

# dictionary containing exit points of map
exits = {0: {"Q": 0},
         1: {"W": 2, "E": 3, "N": 5, "S": 4, "Q": 0},
         2: {"N": 5, "Q": 0},
         3: {"W": 1, "Q": 0},
         4: {"N": 1, "W": 2, "Q": 0},
         5: {"W": 2, "S": 1, "Q": 0},
         }

directions = {"EAST": "E",
              "WEST": "W",
              "NORTH": "N",
              "SOUTH": "S",
              "QUIT": "Q",
             }

# current location of the person
loc = 1
while True:
    if loc == 0:
        break
    print("Your current location : {}".format(locations.get(loc)))
    print("Directions you can choose are: ")
    values = exits.get(loc)
    values_list = list(values.keys())
    print(",".join(values_list))
    way = input("Enter the direction you want to take : ").upper()
    if len(way) == 1:
        if way in values_list:
            loc = values.get(way)
        else:
            print("Your path does not exist.")
    else:
        word_list = way.split(" ")
        for word in word_list:
            # print(word)
            if word in directions:
                path_taken = word
                loc = values.get(directions.get(path_taken))
                break
        else:
            print("Your path does not exist.")
