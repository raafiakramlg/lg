# WE WILL USE WRITING OPERATION HERE

# cities = ["sydney", "osaka", "melbourne", "tokyo"]
#
# with open("/home/raafi/projects/python/Restart(Udemy)/sec-8/L-194/cities.txt","w") as city_file:
#     for city in cities:
#         print(city,file=city_file)

cities = []

with open("/home/raafi/projects/python/Restart(Udemy)/sec-8/L-194/cities.txt", 'r') as city_file:
    for city in city_file:
        cities.append(city.strip("\n"))     # strip method

print(cities)
for city in cities:
    print(city)


# strip method
# it only removes chars from beginning or ending
string = "helloe"

print(string.strip("he"))

print(string.strip("hl"))

print(string.strip("e"))