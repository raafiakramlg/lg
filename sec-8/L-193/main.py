# WE WILL READ AND WRITE TEXT FILE HERE

# get the text file position
text = open("/home/raafi/projects/python/Restart(Udemy)/sec-8/L-193/SampleText", "r")

# for line in text:
#    print(line)
# print("#" * 40)
# for line in text:
#     if "mome" in line.lower():
#         print(line, end="")
# text.close()

# if we dont want to use close operation

# with open("/home/raafi/projects/python/Restart(Udemy)/sec-8/L-193/SampleText", "r") as tim:
#     for line in tim:
#         if "jab" in line.lower():
#             print(line, end="")

# another way. Usually other languages use this way
# with open("/home/raafi/projects/python/Restart(Udemy)/sec-8/L-193/SampleText", "r") as tim:
#     line = tim.readline()
#     while line:
#         print(line, end="")
#         line = tim.readline()

# another way. It prints as a list
# with open("/home/raafi/projects/python/Restart(Udemy)/sec-8/L-193/SampleText", "r") as tim:
#     lines = tim.readlines()
#     print(lines, end="")